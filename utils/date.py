from khayyam import JalaliDatetime


def convert(date):
    jalali = JalaliDatetime(date)
    format_date = '{}/{}/{}'.format(jalali.year, jalali.month, jalali.day)
    format_time = '{}:{}'.format(jalali.hour, jalali.minute)
    return '{} - {}'.format(format_date, format_time)
