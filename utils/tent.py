from guest.models import Tent, Bed


def create_tent_by_count(tent_type, num, capacity):
    last_priority = Tent.objects.filter(
        tent_type=tent_type
    ).order_by('priority').last()
    if not last_priority:
        last_priority = 0
    else:
        last_priority = last_priority.priority

    for i in range(last_priority + 1, last_priority + num + 1):
        tent = Tent.objects.create(
            tent_type=tent_type,
            priority=i,
            capacity=capacity,
        )
        for j in range(capacity):
            Bed.objects.create(tent=tent)
        print('Tent: {}, beds count: {}'.format(tent, tent.beds.count()))
    return True
