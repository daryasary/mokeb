from datetime import timedelta

from django.db.models import Prefetch
from django.utils import timezone
from rest_framework.authentication import SessionAuthentication
from rest_framework.decorators import list_route
from rest_framework.generics import (
    CreateAPIView, ListCreateAPIView,
    RetrieveAPIView, ListAPIView)
from rest_framework.permissions import IsAuthenticated
from rest_framework.viewsets import ModelViewSet
from rest_framework_jwt.authentication import JSONWebTokenAuthentication
from rest_framework.response import Response
from rest_framework import status
from rest_framework.views import APIView

from allocation.models import Allocation
from guest.models import Guest, EntryLog, Bed, Tent, GuestBed
from shift.models import Shift
from shift.serializers import ShiftReportSerializer
from users.models import User
from location.models import State, City
from .serializers import (
    EnterExitSerializer, ForceSettlementCreateSerializer,
    SettlementSerializer, GuestSerializer, BedDetailSerializer,
    ForceSettlementListSerializer, GuestRegisterSerializer,
    VIPGuestRegisterSerializer, VIPBedRegisterSerializer,
    TentDetailSerializer, StateSerializer, SwapSerializer,
    CitySerializer)


class LoginUserViewSet(ModelViewSet):
    authentication_classes = (JSONWebTokenAuthentication, SessionAuthentication)
    permission_classes = (IsAuthenticated,)
    serializer_class = GuestSerializer
    queryset = Guest.objects.prefetch_related(
        Prefetch('logs', queryset=EntryLog.objects.select_related('operator', 'bed', 'bed__tent').order_by('-time'))
    ).select_related('state', 'city').all()
    lookup_field = 'passport_code'

    def get_serializer_class(self):
        if self.request is not None and self.request.method in ['POST', 'PUT']:
            return GuestRegisterSerializer
        return self.serializer_class

    def perform_create(self, serializer):
        serializer.save(operator=self.request.user)

    def perform_update(self, serializer):
        serializer.save(operator=self.request.user)


class LoginVIPUserViewSet(ModelViewSet):
    # permission_classes = (IsAuthenticated, UserPermission)
    authentication_classes = (JSONWebTokenAuthentication,)
    serializer_class = VIPGuestRegisterSerializer
    queryset = Guest.objects.select_related('state', 'city').all()
    lookup_field = 'national_code'

    @list_route(methods=['post'])
    def assign_vip(self, request, *args, **kwargs):
        serializer = VIPBedRegisterSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save(operator=request.user)
        return Response(serializer.data, status=status.HTTP_201_CREATED)


class SettlementAPIView(CreateAPIView):
    # permission_classes = (IsAuthenticated, UserPermission)
    authentication_classes = (JSONWebTokenAuthentication,)
    serializer_class = SettlementSerializer

    def perform_create(self, serializer):
        serializer.save(operator=self.request.user)


class EnterExitAPIView(CreateAPIView):
    level = User.GUARDIAN
    serializer_class = EnterExitSerializer
    # permission_classes = (IsAuthenticated, UserPermission)
    authentication_classes = (JSONWebTokenAuthentication,)

    def perform_create(self, serializer):
        serializer.save(operator=self.request.user)


class GetLateLeftAPIView(ListCreateAPIView):
    level = User.INSPECTOR
    # permission_classes = (IsAuthenticated, UserPermission)
    authentication_classes = (JSONWebTokenAuthentication,)

    def get_serializer_class(self):
        if self.request is not None and self.request.method == 'POST':
            return ForceSettlementCreateSerializer
        else:
            return ForceSettlementListSerializer

    def get_queryset(self):
        margin_time = int(self.kwargs.get('margin', 5))
        margin = timezone.now() - timedelta(hours=margin_time)

        queryset = EntryLog.objects.select_related('bed', 'guest',
                                                   'bed__tent').filter(
            bed__status=Bed.UNAVAILABLE,
            log_type=EntryLog.EXIT,
            time__lte=margin
        )

        entrance = EntryLog.objects.values_list(
            'bed_id', flat=True
        ).filter(
            time__gte=margin, log_type=EntryLog.ENTER
        )

        queryset = queryset.exclude(bed_id__in=entrance)

        return queryset

    def perform_create(self, serializer):
        serializer.save(operator=self.request.user)


class BedDetailView(RetrieveAPIView):
    level = User.INSPECTOR
    # permission_classes = (IsAuthenticated, UserPermission)
    authentication_classes = (JSONWebTokenAuthentication,)
    serializer_class = BedDetailSerializer
    queryset = Bed.objects.all()
    lookup_field = 'title'


class TentDetailView(ListAPIView):
    level = User.INSPECTOR
    # permission_classes = (IsAuthenticated, UserPermission)
    authentication_classes = (JSONWebTokenAuthentication,)
    serializer_class = TentDetailSerializer
    queryset = Tent.objects.all()


class StatesView(ListAPIView):
    serializer_class = StateSerializer
    queryset = State.objects.all()


class CitiesListView(ListAPIView):
    serializer_class = CitySerializer
    queryset = City.objects.all()

    def get_queryset(self):
        sid = self.kwargs['state_id']
        queryset = City.objects.filter(state=sid)
        return queryset


class SwapView(APIView):
    level = User.GUARDIAN
    # permission_classes = (IsAuthenticated, UserPermission)
    authentication_classes = (JSONWebTokenAuthentication,)
    serializer_class = SwapSerializer

    def post(self, request, format=None):
        serialized = self.serializer_class(data=request.data)
        if serialized.is_valid():
            serialized.save(operator=self.request.user)
            return Response(serialized.data, status=status.HTTP_201_CREATED)
        return Response(serialized.errors, status=status.HTTP_400_BAD_REQUEST)


class GetShiftsLate(APIView):
    level = User.INSPECTOR
    # permission_classes = (IsAuthenticated, UserPermission)
    authentication_classes = (JSONWebTokenAuthentication,)

    def get(self, request, *args, **kwargs):
        now = timezone.now()
        current = Shift.objects.filter(
            start_date__lte=now, end_date__gte=now
        ).first()
        # current = Shift.objects.first()

        if current:
            passed_shifts = Shift.objects.filter(
                end_date__lte=current.start_date
            )
            if passed_shifts:
                serializer = ShiftReportSerializer(passed_shifts, many=True)
                return Response(serializer.data, status=status.HTTP_200_OK)
        return Response([], status=status.HTTP_200_OK)


class RemainedBedsView(APIView):
    authentication_classes = (JSONWebTokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def get(self, request):
        aid = request.user.allocation_id
        allocation = Allocation.objects.prefetch_related('scopes').get(pk=aid)
        return Response(allocation.get_stats(), status=200)


class TentForceSettlementView(APIView):
    # authentication_classes = (JSONWebTokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def delete(self, request, tent_id, *args, **kwargs):
        user = request.user

        guests_bed = GuestBed.objects.select_related(
            'tent', 'bed', 'guest').filter(tent_id=tent_id,
                                           guest__white_list=False)

        EntryLog.objects.bulk_create(
            [EntryLog(guest=gb.guest, log_type=4, operator=user, bed=gb.bed) for
             gb in guests_bed])
        beds = Bed.objects.filter(
            pk__in=guests_bed.values_list('bed__id', flat=True))
        beds.update(status=1)
        guests_bed.delete()

        return Response(status=200)
