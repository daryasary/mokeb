from django.db import transaction
from django.utils.translation import ugettext_lazy as _
from rest_framework import serializers
from rest_framework.exceptions import ValidationError

from guest.models import EntryLog, Bed, Guest, Tent, GuestBed
from users.serializers import OperatorSerializer
from location.models import City, State
from utils.date import convert


class LogInlineSerializer(serializers.ModelSerializer):
    operator = OperatorSerializer()
    bed = serializers.SlugRelatedField(slug_field='title', read_only=True)

    class Meta:
        model = EntryLog
        fields = ('time', 'log_type', 'operator', 'bed')


class GuestSerializer(serializers.ModelSerializer):
    status = serializers.SerializerMethodField()
    tent = serializers.SerializerMethodField()
    bed = serializers.SerializerMethodField()
    type = serializers.SerializerMethodField()
    registration_date = serializers.DateTimeField(source='modified_at')
    logs = serializers.SerializerMethodField(read_only=True)
    time = serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = Guest
        exclude = ('created_at', 'modified_at')

    @staticmethod
    def get_status(obj):
        logs = obj.logs.all()
        if logs.exists():
            return logs.first().log_type
        return 0

    @staticmethod
    def get_tent(obj):
        if obj.has_bed:
            return obj.logs.all()[0].bed.tent.title

    @staticmethod
    def get_bed(obj):
        if obj.has_bed:
            return obj.logs.all()[0].bed.title

    @staticmethod
    def get_type(obj):
        if obj.has_bed:
            return obj.logs.all()[0].bed.bed_type
        return 0

    @staticmethod
    def get_logs(obj):
        logs = obj.logs.all()[0:5]
        return LogInlineSerializer(logs, many=True).data

    @staticmethod
    def get_time(obj):
        first_index = obj.logs.all().count() - 1
        last_log = obj.logs.all()[first_index] if first_index > 0 else None
        if last_log:
            return last_log.time
        return None


class BedInlineSerializer(serializers.ModelSerializer):
    class Meta:
        model = Bed
        fields = ('id', 'title', 'bed_type', 'tent', 'status')


class BedDetailSerializer(serializers.ModelSerializer):
    class Meta:
        model = Bed
        fields = ('id', 'title', 'bed_type', 'tent', 'status')

    def to_representation(self, instance):
        ret = super(BedDetailSerializer, self).to_representation(instance)
        ret['guest'] = None
        if instance.status == Bed.UNAVAILABLE:
            query = EntryLog.objects.filter(bed=instance).last()
            try:
                ret['guest'] = GuestSerializer(query.guest).data
            except:
                print(instance.id)
                pass
        return ret


class GuestRegisterSerializer(serializers.ModelSerializer):
    class Meta:
        model = Guest
        exclude = ('created_at', 'modified_at')

    @property
    def request_user(self):
        if 'request' in self.context.keys():
            return self.context['request'].user
        return None

    def validate(self, attrs):
        valid_data = super(GuestRegisterSerializer, self).validate(attrs)
        if not Bed.get_last_available_bed_q(operator=self.request_user):
            raise ValidationError(_('There is no available bed now.'))
        return valid_data

    def create(self, validated_data):
        operator = validated_data.pop('operator')
        guest = super(GuestRegisterSerializer, self).create(validated_data)
        q = Bed.get_last_available_bed_q(operator)
        with transaction.atomic():
            bed = Bed.available.select_for_update().filter(q).first()
            if bed is None:
                raise ValidationError(_('There is not available bed now.'))
            log = EntryLog.objects.create(guest=guest, bed=bed, log_type=EntryLog.ENTER, operator=operator)
            gb = GuestBed.objects.create(guest=guest, bed=bed, tent=bed.tent, log=log)
        return log

    def update(self, instance, validated_data):
        operator = validated_data.pop('operator')
        guest = super(GuestRegisterSerializer, self).update(instance, validated_data)
        if not guest.has_bed:
            if guest.logs.exists():
                raise ValidationError(_("This guest had bed before."))
            q = Bed.get_last_available_bed_q(operator)
            with transaction.atomic():
                bed = Bed.available.select_for_update().filter(q).first()
                if bed is None:
                    raise ValidationError(_('There is not available bed now.'))
                log = EntryLog.objects.create(guest=guest, bed=bed, log_type=EntryLog.ENTER, operator=operator)
                gb = GuestBed.objects.create(guest=guest, bed=bed, tent=bed.tent, log=log)
        else:
            log = EntryLog.objects.filter(guest=guest).last()
        return log

    def to_representation(self, instance):
        if not isinstance(instance, EntryLog):
            return {}
        shift = '    ' + instance.guest.shift.title if instance.guest.shift is not None else ''
        ret = {
            'guest': instance.guest.passport_code,
            'bed': instance.bed.title,
            'tent': instance.bed.tent.title,
            'status': instance.log_type,
            'registration_date': convert(instance.guest.modified_at) + shift,
        }
        return ret


class VIPGuestRegisterSerializer(serializers.ModelSerializer):
    class Meta:
        model = Guest
        exclude = ('created_at', 'modified_at')

    def get_tent_type(self):
        request = self.context.get('request', None)
        if request:
            type_ = Tent.VIP
            if 'staff' in request.path:
                type_ = Tent.STAFF
            return type_

    def to_representation(self, instance):
        ret = super(VIPGuestRegisterSerializer, self). \
            to_representation(instance)
        beds = Bed.available.select_related('tent').filter(tent__tent_type=self.get_tent_type())
        log = instance.logs.last()
        if log:
            ret['bed'] = log.bed.title
            ret['tent'] = log.bed.tent.title
            ret['status'] = log.log_type
        else:
            ret['bed'] = None
            ret['tent'] = None
            ret['status'] = 0

        ret['availables'] = BedInlineSerializer(beds, many=True).data
        ret['registration_date'] = instance.modified_at
        return ret


class VIPBedRegisterSerializer(serializers.ModelSerializer):
    class Meta:
        model = EntryLog
        fields = ('guest', 'bed')

    def validate(self, attrs):
        if attrs['bed'].status == Bed.UNAVAILABLE:
            raise ValidationError(
                _("Sorry selected bed is not available currently")
            )
        if attrs['guest'].has_bed:
            raise ValidationError(
                _("Selected user already has bed")
            )
        attrs['log_type'] = EntryLog.ENTER
        return attrs


class EnterExitSerializer(serializers.ModelSerializer):
    guest = serializers.CharField(max_length=50)

    class Meta:
        model = EntryLog
        fields = ('guest',)

    def validate(self, attrs):
        guest = Guest.objects.filter(national_code=attrs['guest']).first()
        if not guest:
            raise ValidationError(
                _('Guest with provided data does not exists.')
            )
        attrs['guest'] = guest
        if not guest.has_bed:
            raise ValidationError(_('Guest doesnt have any bed'))
        return attrs

    def create(self, validated_data):
        last_log = EntryLog.objects.filter(guest=validated_data['guest']).last()
        status = EntryLog.EXIT
        if last_log.log_type == status:
            status = EntryLog.ENTER
        new_log = EntryLog.objects.create(
            guest=validated_data['guest'],
            log_type=status,
            operator=validated_data['operator'],
            bed=validated_data['guest'].bed
        )
        return new_log


class SettlementSerializer(serializers.ModelSerializer):
    guest = serializers.CharField(max_length=50)

    class Meta:
        model = EntryLog
        fields = ('guest',)

    def validate(self, attrs):
        guest = Guest.objects.filter(passport_code=attrs['guest']).first()
        if not guest:
            raise ValidationError(_('Guest with provided data does not exists.'))
        if not guest.has_bed:
            raise ValidationError(_('Guest doesnt have any bed'))

        attrs['guest'] = guest
        attrs['bed'] = guest.bed
        attrs['log_type'] = EntryLog.SETTLEMENT
        return attrs

    def create(self, validated_data):
        with transaction.atomic():
            instance = EntryLog.objects.create(**validated_data)
            GuestBed.objects.filter(guest=validated_data['guest']).delete()
        return instance


class ForceSettlementListSerializer(serializers.ModelSerializer):
    first_name = serializers.CharField(source='guest.first_name',
                                       read_only=True)
    last_name = serializers.CharField(source='guest.last_name',
                                      read_only=True)
    national_code = serializers.CharField(source='guest.national_code',
                                          read_only=True)
    tent = serializers.CharField(source='bed.tent.title',
                                 read_only=True)
    bed = serializers.CharField(source='bed.title',
                                read_only=True)
    profile_picture = serializers.CharField(source='guest.profile_picture',
                                            read_only=True)
    status = serializers.IntegerField(source='log_type',
                                      read_only=True)
    registration_date = serializers.DateTimeField(source='guest.modified_at',
                                                  read_only=True)

    class Meta:
        model = EntryLog
        fields = (
            'time',
            'first_name',
            'last_name',
            'national_code',
            'tent',
            'bed',
            'profile_picture',
            'status',
            'registration_date'
        )


class ForceSettlementCreateSerializer(serializers.ModelSerializer):
    guest = serializers.CharField(max_length=50)

    class Meta:
        model = EntryLog
        fields = ('guest',)

    def validate(self, attrs):
        guest = Guest.objects.filter(national_code=attrs['guest']).first()
        if not guest:
            raise ValidationError(
                _('Guest with provided data does not exists.')
            )
        if not guest.has_bed:
            raise ValidationError(_("Guest doesn't have any bed"))

        attrs['guest'] = guest
        attrs['bed'] = guest.bed
        attrs['log_type'] = EntryLog.SETTLEMENT
        return attrs

    def create(self, validated_data):
        with transaction.atomic():
            instance = EntryLog.objects.create(**validated_data)
            GuestBed.objects.filter(guest=validated_data['guest']).delete()
        return instance


class TentDetailSerializer(serializers.ModelSerializer):
    beds = serializers.SerializerMethodField()

    class Meta:
        model = Tent
        fields = (
            'title', 'priority', 'capacity',
            'tent_type', 'beds',
        )

    def get_beds(self, obj):
        beds = obj.beds.all()
        return BedDetailSerializer(beds, many=True).data

    def to_representation(self, instance):
        ret = super(TentDetailSerializer, self).to_representation(instance)
        ret['available'] = Bed.available.filter(tent=instance).count()
        ret['unavailable'] = Bed.unavailable.filter(tent=instance).count()
        ret['suspended'] = Bed.objects.filter(
            tent=instance, status=Bed.SUSPENDED
        ).count()
        return ret


class CitySerializer(serializers.ModelSerializer):
    class Meta:
        model = City
        fields = ('id', 'city_name',)


class StateSerializer(serializers.ModelSerializer):
    cities = serializers.SerializerMethodField()

    class Meta:
        model = State
        fields = ('id', 'state_name', 'cities')

    def get_cities(self, obj):
        query = obj.city_set.all()
        return CitySerializer(query, many=True).data


class SwapSerializer(serializers.ModelSerializer):
    source = serializers.CharField(max_length=16)
    destination = serializers.CharField(max_length=16)

    class Meta:
        model = EntryLog
        fields = (
            'time', 'source', 'destination'
        )

    def validate(self, attrs):
        try:
            source = Bed.objects.select_related('tent').get(
                title=attrs['source']
            )
            destination = Bed.objects.select_related('tent').get(
                title=attrs['destination']
            )
        except Bed.DoesNotExist:
            raise ValidationError(_('invalid bed titles'))

        if Bed.available.filter(id=source.id).exists():
            raise ValidationError(_('source doesnt have any guest'))

        if not source.tent.tent_type == destination.tent.tent_type:
            raise ValidationError(_('swap is only for guests'))

        # May cause to low speed response
        # dst_log = destination.logs.last()
        # if dst_log is not None:
        #     if dst_log.log_type == EntryLog.EXIT:
        #         raise ValidationError(_('guest of destination bed is absent'))
        #
        # if source.logs.last().log_type == EntryLog.EXIT:
        #     raise ValidationError(_('guest of source bed is absent'))

        if Bed.SUSPENDED in (source.status, destination.status):
            raise ValidationError(_('you select a suspended bed'))

        attrs['source'] = source
        attrs['destination'] = destination
        return attrs

    def create(self, validated_data):
        source = validated_data['source']
        destination = validated_data['destination']
        with transaction.atomic():
            if destination.status == Bed.UNAVAILABLE:
                destination_log = EntryLog.objects.create(
                    guest=destination.guest,
                    log_type=EntryLog.SWAP,
                    operator=validated_data['operator'],
                    bed=source
                )
                destination_gb = GuestBed.objects.create(
                    tent=source.tent, bed=source,
                    guest=destination.guest, log=destination_log
                )
            else:
                source.set_available()
                destination.set_unavailable()
            source_log = EntryLog.objects.create(
                guest=source.guest,
                log_type=EntryLog.SWAP,
                operator=validated_data['operator'],
                bed=destination
            )
            source_gb = GuestBed.objects.create(
                tent=destination.tent, bed=destination,
                guest=source.guest, log=source_log
            )
        return source_log

    def to_representation(self, instance):
        guest = GuestSerializer(instance=instance.guest)
        ret = {
            'guest': guest.data,
            'time': instance.time,
            'bed': instance.bed.title,
            'tent': instance.bed.tent.title,
        }
        return ret
