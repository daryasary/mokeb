from django.conf.urls import url
from rest_framework import routers

from api.views import (LoginUserViewSet, SettlementAPIView, SwapView,
                       GetLateLeftAPIView, EnterExitAPIView,
                       BedDetailView, TentDetailView, StatesView,
                       LoginVIPUserViewSet, CitiesListView, GetShiftsLate,
                       RemainedBedsView, TentForceSettlementView)

urlpatterns = [
    url(r'^settle_bed/$', SettlementAPIView.as_view(), name='settlement_bed'),
    url(r'^gate_pass/$', EnterExitAPIView.as_view(), name='gate_pass'),
    url(r'^get_late_entrance/(?P<margin>.*)/$', GetLateLeftAPIView.as_view(), name='assign_bed_vip'),
    url(r'^get_bed/(?P<title>.*)/$', BedDetailView.as_view(), name='bed_detail'),
    url(r'^get_tent/$', TentDetailView.as_view(), name='tent_detail'),
    url(r'^get_states/$', StatesView.as_view(), name='state_views'),
    url(r'^get_cities/(?P<state_id>.*)/$', CitiesListView.as_view(), name='city_views'),
    url(r'^swap_bed/$', SwapView.as_view(), name='swap_bed'),
    url(r'^shifts_report/$', GetShiftsLate.as_view(), name='shifts_report'),

    url(r'^remained/$', RemainedBedsView.as_view(), name='remain_beds_view'),
    url(r'^tents/(?P<tent_id>\d+)/$', TentForceSettlementView.as_view(), name='tent_force_settlement_view'),
]

router = routers.SimpleRouter()
router.register(r'guests', LoginUserViewSet)
router.register(r'vip', LoginVIPUserViewSet)
router.register(r'staff', LoginVIPUserViewSet)
urlpatterns += router.urls
