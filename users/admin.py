from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.utils.translation import ugettext_lazy as _

from .models import User


class MyUserAdmin(UserAdmin):
    fieldsets = (
        (None, {'fields': ('username', 'password', 'role', 'allocation')}),
        (_('Personal info'),
         {'fields': ('first_name', 'last_name', 'phone_number', 'email')}),
        (_('Permissions'), {'fields': ('is_active', 'is_staff', 'is_superuser',
                                       'groups', 'user_permissions')}),
        (_('Important dates'), {'fields': ('last_login', 'date_joined')}),
    )
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': (
                'username', 'phone_number', 'password1',
                'password2', 'role', 'allocation'
            ),
        }),
    )
    list_display = ('username', 'phone_number', 'role', 'allocation', 'is_staff')
    search_fields = ('username', 'phone_number')


admin.site.register(User, MyUserAdmin)
