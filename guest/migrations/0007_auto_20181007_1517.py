# -*- coding: utf-8 -*-
# Generated by Django 1.11.5 on 2018-10-07 11:47
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('guest', '0006_guest_white_list'),
    ]

    operations = [
        migrations.AlterModelTable(
            name='bed',
            table='bed',
        ),
        migrations.AlterModelTable(
            name='entrylog',
            table='entry_log',
        ),
        migrations.AlterModelTable(
            name='guest',
            table='guest',
        ),
        migrations.AlterModelTable(
            name='tent',
            table='tent',
        ),
    ]
