# -*- coding: utf-8 -*-
# Generated by Django 1.11.4 on 2017-09-05 13:36
from __future__ import unicode_literals

import django.core.validators
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('location', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Bed',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('bed_type', models.IntegerField(choices=[(1, 'Real Bed'), (2, 'Implicit Bed')], default=1)),
                ('status', models.IntegerField(choices=[(1, 'Available'), (2, 'Unavailable'), (3, 'Suspended')], default=1)),
                ('title', models.CharField(editable=False, max_length=16)),
                ('type_index', models.IntegerField(editable=False)),
            ],
            options={
                'verbose_name': 'bed',
                'verbose_name_plural': 'beds',
            },
        ),
        migrations.CreateModel(
            name='EntryLog',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('time', models.DateTimeField(auto_now_add=True, verbose_name='time')),
                ('log_type', models.IntegerField(choices=[(1, 'Enter'), (2, 'Exit'), (3, 'Settlement'), (4, 'Force Settlement'), (5, 'Swap')], verbose_name='log type')),
                ('bed', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='logs', to='guest.Bed', verbose_name='bed')),
            ],
            options={
                'verbose_name': 'log',
                'verbose_name_plural': 'logs',
            },
        ),
        migrations.CreateModel(
            name='Guest',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('first_name', models.CharField(max_length=50, verbose_name='First name')),
                ('last_name', models.CharField(max_length=50, verbose_name='Last name')),
                ('passport_code', models.BigIntegerField(unique=True, verbose_name='Passport code')),
                ('national_code', models.BigIntegerField(unique=True, verbose_name='National code')),
                ('profile_picture', models.ImageField(blank=True, null=True, upload_to='profile_pic', verbose_name='Profile pic')),
                ('passport_picture', models.ImageField(blank=True, null=True, upload_to='passport_pic', verbose_name='Passport pic')),
                ('mobile_number', models.BigIntegerField(error_messages={'unique': 'A user with this mobile number already exists.'}, unique=True, validators=[django.core.validators.RegexValidator('^9[0-3,9]\\d{8}$', 'Enter a valid mobile number.', 'invalid')], verbose_name='Mobile number')),
                ('phone_number', models.BigIntegerField(blank=True, null=True, verbose_name='Home phone number')),
                ('birthday', models.CharField(blank=True, max_length=64, null=True, verbose_name='Birth date')),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='Registered')),
                ('modified_at', models.DateTimeField(auto_now=True, verbose_name='Modified')),
                ('city', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='location.City', verbose_name='City')),
                ('state', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='location.State', verbose_name='State')),
            ],
            options={
                'verbose_name': 'guest',
                'verbose_name_plural': 'guests',
            },
        ),
        migrations.CreateModel(
            name='Tent',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('tent_type', models.IntegerField(choices=[(3, 'Vip'), (2, 'Staff'), (1, 'Guest')], default=1, verbose_name='type of tent')),
                ('priority', models.PositiveIntegerField(verbose_name='priority')),
                ('capacity', models.PositiveIntegerField(verbose_name='capacity')),
            ],
            options={
                'ordering': ['priority'],
                'verbose_name': 'tent',
                'verbose_name_plural': 'tents',
            },
        ),
        migrations.AlterUniqueTogether(
            name='tent',
            unique_together=set([('tent_type', 'priority')]),
        ),
        migrations.AddField(
            model_name='entrylog',
            name='guest',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='logs', to='guest.Guest', verbose_name='guest'),
        ),
    ]
