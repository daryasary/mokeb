# -*- coding: utf-8 -*-
# Generated by Django 1.11.9 on 2018-10-08 07:52
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('guest', '0010_auto_20181008_1111'),
    ]

    operations = [
        migrations.AlterField(
            model_name='guestbed',
            name='guest',
            field=models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, related_name='guest_bed', to='guest.Guest'),
        ),
    ]
