# -*- coding: utf-8 -*-
# Generated by Django 1.11.5 on 2018-10-07 09:14
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('guest', '0005_guest_printed'),
    ]

    operations = [
        migrations.AddField(
            model_name='guest',
            name='white_list',
            field=models.BooleanField(default=False),
        ),
    ]
