from django.db import models


class BedAvailableManager(models.Manager):
    def get_queryset(self):
        return super(BedAvailableManager, self).get_queryset().filter(status=1)


class BedUnAvailableManager(models.Manager):
    def get_queryset(self):
        return super(BedUnAvailableManager, self).get_queryset().filter(status=2)
