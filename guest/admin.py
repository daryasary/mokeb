# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin, messages
from django.http import HttpResponse
from django.utils.translation import ugettext_lazy as _
from import_export.admin import ExportActionModelAdmin

from guest.models import *
from panel.admin import TentResource


class EntryLogInline(admin.TabularInline):
    model = EntryLog
    extra = 0

    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False


class GuestAdmin(admin.ModelAdmin):
    list_display = ('fullname', 'passport_code', 'mobile_number', 'accepted_terms', 'created_at')
    search_fields = ('national_code', 'first_name', 'last_name', 'passport_code', 'mobile_number')
    actions = ('add_to_whitelist',)
    inlines = (EntryLogInline, )

    fields = (
        ("first_name", "last_name", "passport_code", ),
        ("national_code", "mobile_number", "phone_number", "birthday"),
        ("profile_picture", "passport_picture"),
        ("state", "city", "printed", "white_list", "accepted_terms"),
    )

    def add_to_whitelist(self, request, queryset):
        queryset.update(white_list=True)
    add_to_whitelist.short_description = _("Add selected users to whitelist")


class TentAdmin(ExportActionModelAdmin):
    list_display = (
        'tent_type', 'capacity', 'priority', 'title'
    )
    list_editable = ('priority',)
    list_filter = ('tent_type',)
    resource_class = TentResource

    actions = ('export_tent_guests',)

    def get_actions(self, request):
        actions = super().get_actions(request)
        del actions["delete_selected"]
        del actions["export_admin_action"]
        return actions

    def export_tent_guests(self, request, queryset):
        gb_queryset = GuestBed.objects.select_related(
            'tent', 'bed', 'guest'
        ).filter(tent=queryset.first().id).order_by('bed')
        if queryset.count() > 1:
            messages.warning(request, _('Multiple tent selected'))
        else:
            formats = self.get_export_formats()
            file_format = formats[int(0)]()

            export_data = self.get_export_data(file_format, gb_queryset, request=request)
            content_type = file_format.get_content_type()
            response = HttpResponse(export_data, content_type=content_type)
            response['Content-Disposition'] = 'attachment; filename=%s' % (
                self.get_export_filename(file_format),
            )
            return response
    export_tent_guests.short_description = _('Export selected tent guests')


class BedAdmin(admin.ModelAdmin):
    list_display = (
        'tent', 'bed_type', 'status', 'title'
    )
    list_filter = ('status', )


class EntryLogAdmin(admin.ModelAdmin):
    list_display = ('guest', 'log_type', 'time', 'operator', 'bed')
    list_filter = ('log_type', )

    search_fields = (
        'guest__first_name', 'guest__last_name',
        'guest__national_code', 'guest__passport_code',
        'guest__mobile_number', 'operator__first_name',
        'operator__last_name', 'operator__username',
    )

    def has_delete_permission(self, request, obj=None):
        return False


class GuestBedAdmin(admin.ModelAdmin):
    list_display = ('tent', 'bed', 'guest')
    raw_id_fields = ('bed', 'guest', 'log')


admin.site.register(Guest, GuestAdmin)
admin.site.register(Tent, TentAdmin)
admin.site.register(Bed, BedAdmin)
admin.site.register(EntryLog, EntryLogAdmin)
admin.site.register(GuestBed, GuestBedAdmin)
