from django.db.models.signals import post_save
from django.dispatch import receiver

from guest.models import EntryLog


@receiver(post_save, sender=EntryLog)
def check_tent_status(sender, instance, created, **kwargs):
    if instance.log_type == EntryLog.ENTER:
        instance.bed.set_unavailable()

    if instance.log_type in [EntryLog.SETTLEMENT, EntryLog.FORCE_SETTLEMENT]:
        instance.bed.set_available()
