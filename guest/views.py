# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.http import Http404
from django.views.generic import DetailView, ListView
from django.views.generic.edit import CreateView
from django.utils.translation import ugettext_lazy as _
from django.shortcuts import get_object_or_404

from .models import Guest
from .forms import RegisterForm


class GuestRegister(CreateView):
    form_class = RegisterForm
    template_name = 'guest/guest_form.html'


class GuestDetail(DetailView):
    model = Guest
    lookup = 'passport_code'
    queryset = Guest.objects.select_related('city', 'state')

    def get_object(self, queryset=None):
        if queryset is None:
            queryset = self.get_queryset()

        passport_code = self.kwargs.get(self.lookup)
        if passport_code is not None:
            queryset = queryset.filter(passport_code=passport_code)

        try:
            # Get the single item from the filtered queryset
            obj = queryset.get()
        except queryset.model.DoesNotExist:
            raise Http404(_("No %(verbose_name)s found matching the query") %
                          {'verbose_name': queryset.model._meta.verbose_name})
        return obj
