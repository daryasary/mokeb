# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import os
import barcode

from django.conf import settings
from django.core import validators
from django.db import models
from django.utils.translation import ugettext_lazy as _


from location.models import State, City
from shift.models import Shift
from .manager import BedAvailableManager, BedUnAvailableManager


class Guest(models.Model):
    NORMAL = 1
    STAFF = 2
    VIP = 3

    first_name = models.CharField(verbose_name=_('First name'), max_length=50)
    last_name = models.CharField(verbose_name=_('Last name'), max_length=50)
    passport_code = models.CharField(verbose_name=_('Passport code'), unique=True, max_length=10, db_index=True)
    national_code = models.CharField(verbose_name=_('National code'), blank=True, null=True, max_length=10, db_index=True)
    profile_picture = models.ImageField(verbose_name=_('Profile pic'), upload_to='profile_pic', blank=True, null=True)
    passport_picture = models.ImageField(verbose_name=_('Passport pic'), upload_to='passport_pic', blank=True, null=True)
    mobile_number = models.BigIntegerField(
        verbose_name=_('Mobile number'), unique=True,
        validators=[validators.RegexValidator(r'^9[0-3,9]\d{8}$', _('Enter a valid mobile number.'), 'invalid')],
        error_messages={'unique': _("A user with this mobile number already exists.")}
    )
    phone_number = models.BigIntegerField(verbose_name=_('Home phone number'), null=True, blank=True,)
    state = models.ForeignKey(State, verbose_name=_('State'), null=True, blank=True)
    city = models.ForeignKey(City, verbose_name=_('City'), null=True, blank=True)
    birthday = models.CharField(verbose_name=_('Birth date'), max_length=64, blank=True, null=True)

    created_at = models.DateTimeField(verbose_name=_('Registered'), auto_now_add=True)
    modified_at = models.DateTimeField(verbose_name=_('Modified'), auto_now=True)
    printed = models.BooleanField(default=False, verbose_name=_("card printed"))
    white_list = models.BooleanField(default=False, verbose_name=_("white list"))
    accepted_terms = models.BooleanField(default=False, verbose_name=_("accepted_terms"))

    @property
    def first_enter_datetime(self):
        datetime = self.logs.filter(log_type=EntryLog.ENTER).order_by('time').first()
        if datetime:
            return datetime.time

    @property
    def barcode(self):
        path = 'media/barcode/'
        if not os.path.exists(path):
            os.makedirs(path)
        path += self.passport_code + '.svg'
        if not os.path.exists(path):
            ean = barcode.get('code39', self.passport_code)
            ean.save(path[:-4])
        return path

    @property
    def has_bed(self):
        return hasattr(self, 'guest_bed')

    @property
    def bed(self):
        if self.has_bed:
            return self.guest_bed.bed

    @property
    def type(self):
        if self.has_bed:
            return self.bed.bed_type

    @property
    def shift(self):
        if self.first_enter_datetime:
            shift = Shift.objects.filter(
                start_date__lte=self.first_enter_datetime, end_date__gte=self.first_enter_datetime
            ).first()
            return shift
        return None

    @property
    def fullname(self):
        return '{} {}'.format(self.first_name, self.last_name)

    def get_absolute_url(self):
        return "guest/%s/" % self.passport_code

    def __str__(self):
        return "%s %s %s" % (self.first_name, self.last_name, self.passport_code)

    class Meta:
        verbose_name = _('guest')
        verbose_name_plural = _('guests')
        db_table = 'guest'


class Tent(models.Model):
    GUEST = 1
    STAFF = 2
    VIP = 3

    TYPE_CHOICE = (
        (VIP, _('Vip')),
        (STAFF, _('Staff')),
        (GUEST, _('Guest')),
    )

    tent_type = models.IntegerField(choices=TYPE_CHOICE, default=GUEST, verbose_name=_('type of tent'),)
    priority = models.PositiveIntegerField(verbose_name=_('priority'))
    capacity = models.PositiveIntegerField(verbose_name=_('capacity'))

    def __str__(self):
        return '%s %s' % (self.get_tent_type_display(), self.priority)

    @property
    def title(self):
        title = {1: 'G', 2: 'S', 3: 'V',}
        return '%s%s' % (title[self.tent_type], self.priority)

    class Meta:
        ordering = ['priority']
        unique_together = ('tent_type', 'priority')
        verbose_name = _('tent')
        verbose_name_plural = _('tents')
        db_table = 'tent'


class Bed(models.Model):
    REAL_BED = 1
    IMPLICIT_BED = 2

    AVAILABLE = 1
    UNAVAILABLE = 2
    SUSPENDED = 3

    BED_CHOICE = (
        (REAL_BED, _('Real Bed')),
        (IMPLICIT_BED, _('Implicit Bed')),
    )

    STATUS_CHOICE = (
        (AVAILABLE, _('Available')),
        (UNAVAILABLE, _('Unavailable')),
        (SUSPENDED, _('Suspended')),
    )

    bed_type = models.IntegerField(choices=BED_CHOICE, default=REAL_BED)
    tent = models.ForeignKey(Tent, on_delete=models.CASCADE, related_name='beds')
    status = models.IntegerField(choices=STATUS_CHOICE, default=AVAILABLE)
    title = models.CharField(max_length=16, editable=False, db_index=True)
    type_index = models.IntegerField(editable=False)

    objects = models.Manager()
    available = BedAvailableManager()
    unavailable = BedUnAvailableManager()

    def __str__(self):
        return '%s in tent %s,id %s' % (self.get_bed_type_display(), self.tent.id, self.id)

    @classmethod
    def get_last_available_bed_q(cls, operator, tent_type=1):
        allocation = operator.allocation
        return allocation.get_available_bed(tent_type)

    @property
    def guest(self):
        if hasattr(self, 'guest_bed'):
            return self.guest_bed.guest

    def save(self, *args, **kwargs):
        if not self.id:
            self.title, self.type_index = self.set_static()
        super(Bed, self).save(*args, **kwargs)

    def set_available(self):
        self.status = self.AVAILABLE
        self.save()

    def set_unavailable(self):
        self.status = self.UNAVAILABLE
        self.save()

    def set_static(self):
        count = Bed.objects.filter(tent=self.tent).count() + 1
        title = '%s%s' % (self.tent.title, str(count).zfill(3))
        return title, count

    class Meta:
        verbose_name = _('bed')
        verbose_name_plural = _('beds')
        db_table = 'bed'


class EntryLog(models.Model):
    ENTER = 1
    EXIT = 2
    SETTLEMENT = 3
    FORCE_SETTLEMENT = 4
    SWAP = 5
    ENTER_CHOICES = (
        (ENTER, _('Enter')),
        (EXIT, _('Exit')),
        (SETTLEMENT, _('Settlement')),
        (FORCE_SETTLEMENT, _('Force Settlement')),
        (SWAP, _('Swap')),
    )
    guest = models.ForeignKey(Guest, on_delete=models.CASCADE, related_name='logs', verbose_name=_('guest'))
    time = models.DateTimeField(auto_now_add=True, verbose_name=_('time'), )
    log_type = models.IntegerField(choices=ENTER_CHOICES, verbose_name=_('log type'))
    operator = models.ForeignKey(
        settings.AUTH_USER_MODEL, on_delete=models.CASCADE, related_name='logs', verbose_name=_('operator')
    )
    bed = models.ForeignKey(Bed, on_delete=models.CASCADE, related_name='logs', verbose_name=_('bed'))

    class Meta:
        verbose_name = _('log')
        verbose_name_plural = _('logs')
        db_table = 'entry_log'


class GuestBed(models.Model):
    """Tmp table for storing bed and guest relationship and normalize data reading
    this table will modify after assign_bed, settle_bed, shift_bed, force_settle"""
    tent = models.ForeignKey(Tent, related_name='guests_beds')
    bed = models.OneToOneField(Bed, related_name='guest_bed')
    guest = models.OneToOneField(Guest, related_name='guest_bed')
    log = models.OneToOneField(EntryLog, null=True)

    class Meta:
        db_table = 'guest_bed'
        verbose_name = _("Guests Bed")
        verbose_name_plural = _("Guests Beds")
