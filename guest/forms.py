from django.forms import ModelForm, HiddenInput
from guest.models import Guest
from location.models import State, City


class RegisterForm(ModelForm):

    def __init__(self, *args, **kwargs):
        super(RegisterForm, self).__init__(*args, **kwargs)
        self.fields['first_name'].widget.attrs = {'class': 'form-control'}
        self.fields['last_name'].widget.attrs = {'class': 'form-control'}
        self.fields['passport_code'].widget.attrs = {'class': 'form-control'}
        # self.fields['national_code'].widget.attrs = {'class': 'form-control'}
        self.fields['mobile_number'].widget.attrs = {'class': 'form-control'}
        # self.fields['phone_number'].widget.attrs = {'class': 'form-control'}
        # self.fields['city'].widget.attrs = {'class': 'form-control'}
        # self.fields['state'].widget.attrs = {'class': 'form-control'}

        self.fields['first_name'].label = u'نام'
        self.fields['last_name'].label = u'نام خانوادگی'
        self.fields['passport_code'].label = u'شماره گذرنامه'
        # self.fields['national_code'].label = u'کد ملی'
        self.fields['mobile_number'].label = u'شماره موبایل'
        # self.fields['phone_number'].label = u'شماره تلفن'
        # self.fields['city'].label = u'شهر'
        # self.fields['state'].label = u'استان'

    def save(self, commit=True):
        instance = super(RegisterForm, self).save(False)
        if commit:
            instance.save()
        return instance

    class Meta:
        model = Guest
        fields = ('first_name', 'last_name', 'passport_code', 'mobile_number')
