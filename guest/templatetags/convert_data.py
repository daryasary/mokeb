from django import template

from khayyam import JalaliDatetime

register = template.Library()


@register.simple_tag()
def convert(date):
    Jalai = JalaliDatetime(date)
    format_date = '/'.join(
        [str(i) for i in [Jalai.day, Jalai.monthname(), Jalai.year]]
    )
    format_time = '%s:%s' % (Jalai.hour, Jalai.minute)
    return ' %s - %s' % (format_date, format_time)
