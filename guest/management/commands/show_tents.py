from django.core.management.base import BaseCommand

from guest.models import Tent


class Command(BaseCommand):

    def handle(self, *args, **options):
        tents = Tent.objects.order_by('priority')
        message = "Tent: {}    Pri: {}    Beds: {}"
        for tent in tents:
            print(message.format(tent.title, tent.priority, tent.beds.count()))
