from django.core.management.base import BaseCommand

from guest.models import Tent, Bed


class Command(BaseCommand):
    help = 'manage.py make_tent [number of tents] [type of tents] [number of beds]'

    def add_arguments(self, parser):
        parser.add_argument('num', type=int)
        parser.add_argument('tent_type', type=int, choices=(1, 2, 3))
        parser.add_argument('capacity', type=int)

    @staticmethod
    def create_tent_by_count(tent_type, capacity, **kwargs):
        message = "Tent: {}    Pri: {}    Beds: {}"
        last_priority = Tent.objects.filter(
            tent_type=tent_type
        ).order_by('priority').last()
        if not last_priority:
            last_priority = 0
        else:
            last_priority = last_priority.priority

        tent = Tent.objects.create(
            tent_type=tent_type, priority=last_priority+1, capacity=capacity,
        )
        for j in range(capacity):
            Bed.objects.create(tent=tent)
        print(message.format(tent, tent.priority, tent.beds.count()))

    def handle(self, *args, **options):
        for _ in range(options['num']):
            self.create_tent_by_count(**options)
