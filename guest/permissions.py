from rest_framework import permissions

from users.models import User


class UserPermission(permissions.BasePermission):
    def has_permission(self, request, view):
        level = User.GUARDIAN
        stream = request.path.split('/')
        if 'vip' in stream:
            level = User.VIP_INSPECTOR
        return request.user.role >= level
