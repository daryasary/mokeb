from django.db import models
from django.db.models import Q, Count
from django.utils.translation import ugettext_lazy as _

from guest.models import Tent, Bed


class Allocation(models.Model):
    title = models.CharField(
        max_length=64,
        verbose_name=_('title'),
    )

    def __str__(self):
        return self.title

    def get_stats(self):
        queries = [Q(
            tent_id=scope['tent_id'],
            type_index__in=range(scope['start'], scope['stop'] + 1)
        ) for scope in self.scopes.values('tent_id', 'start', 'stop')]
        if len(queries):
            q = Q()
            for query in queries:
                q |= query
            return Bed.objects.filter(q).values('status').annotate(
                count=Count('status')
            ).order_by('status')
        return []

    def get_available_bed(self, tent_type):
        queries = [Q(
            tent_id=scope['tent_id'],
            tent__tent_type=tent_type,
            type_index__in=range(scope['start'], scope['stop'] + 1)
        ) for scope in self.scopes.values('tent_id', 'start', 'stop')]
        if len(queries):
            q = Q()
            for query in queries:
                q |= query
            return q
        return None

    class Meta:
        verbose_name = _('allocation')
        verbose_name_plural = _('allocations')
        db_table = 'allocation'


class Scope(models.Model):
    title = models.CharField(max_length=24, verbose_name=_('Title'))
    tent = models.ForeignKey(Tent, related_name='scopes',
                             verbose_name=_('tent'))
    start = models.PositiveIntegerField(verbose_name=_('first bed'), )
    stop = models.PositiveIntegerField(verbose_name=_('last bed'), )
    allocation = models.ManyToManyField(
        Allocation, related_name='scopes',
        verbose_name=_('allocation'), blank=True
    )

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = _('scope')
        verbose_name_plural = _('scopes')
        db_table = 'scope'
