from django.contrib import admin

from allocation.models import Allocation, Scope
from django.utils.translation import ugettext_lazy as _


class ScopeInLine(admin.TabularInline):
    model = Scope.allocation.through
    raw_id_fields = ['scope']


class AllocationAdmin(admin.ModelAdmin):
    inlines = (ScopeInLine, )
    list_display = ('title', 'scopes_title')

    def scopes_title(self, obj):
        return ', '.join(obj.scopes.values_list('title', flat=True))
    scopes_title.short_description = _("Scopes")


class ScopeAdmin(admin.ModelAdmin):
    list_display = ('title', 'tent', 'start', 'stop',)


admin.site.register(Allocation, AllocationAdmin)
admin.site.register(Scope, ScopeAdmin)
