from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _


class AllocationConfig(AppConfig):
    name = 'allocation'
    verbose_name = _('allocation')
