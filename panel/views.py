from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import UserPassesTestMixin
from django.db import transaction
from django.shortcuts import redirect
from django.urls import reverse
from django.utils.decorators import method_decorator
from django.views.generic import DetailView, ListView
from django.views.generic.base import TemplateView
from django.db.models import Count

from rest_framework.generics import ListAPIView, get_object_or_404

from django.utils.translation import ugettext_lazy as _

from guest.models import GuestBed, Tent, Bed, Guest, EntryLog
from allocation.models import Allocation
from .serializers import AllocationLiveSerializer


class PanelMainView(TemplateView):
    """Main view of panel should provide a list of all available urls for web operator of system
    urls may modify manually"""

    template_name = 'panel/panel_main.html'


class TentListView(UserPassesTestMixin, ListView):
    """Return a list of all tents + capacity status + details link"""
    model = Tent
    queryset = Tent.objects.prefetch_related(
        'guests_beds'
    ).annotate(unavailable_beds_count=Count('guests_beds')).order_by('id')
    template_name = 'panel/tents.html'
    context_object_name = 'tents'
    login_url = '/clinic/'

    def test_func(self):
        return self.request.user.is_superuser

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(TentListView, self).dispatch(*args, **kwargs)


class TentDetailView(UserPassesTestMixin, DetailView):
    """Return bed report for selected tent + gusts detail"""
    model = GuestBed
    queryset = GuestBed.objects.select_related('guest', 'bed', 'tent')
    slug_field = 'tent_id'
    template_name = 'panel/tent.html'
    context_object_name = 'guest_bed'
    login_url = '/clinic/'

    def test_func(self):
        return self.request.user.is_superuser

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(TentDetailView, self).dispatch(*args, **kwargs)

    def get_object(self, queryset=None):
        if queryset is None:
            queryset = self.get_queryset()

        slug = self.kwargs.get(self.slug_url_kwarg)
        slug_field = self.get_slug_field()
        queryset = queryset.filter(**{slug_field: slug})
        return queryset

    def get_context_data(self, **kwargs):
        ret = super().get_context_data(**kwargs)
        ret['beds'] = Bed.objects.filter(tent_id=self.kwargs.get('slug'))
        ret['tent_id'] = self.kwargs.get('slug')
        return ret


class AllocationLiveListView(ListAPIView):
    serializer_class = AllocationLiveSerializer
    queryset = Allocation.objects.prefetch_related('scopes')


class AllocationView(UserPassesTestMixin, TemplateView):
    template_name = 'panel/allocations.html'
    login_url = '/clinic/'

    def test_func(self):
        return self.request.user.is_superuser


class UnPrintedGuests(UserPassesTestMixin, ListView):
    model = Guest
    queryset = Guest.objects.filter(printed=False, accepted_terms=True).order_by('created_at')
    template_name = 'panel/guest_unprinted.html'
    login_url = '/clinic/'

    def test_func(self):
        return self.request.user.is_superuser


class UnPrintedGuestDetail(DetailView):
    model = Guest
    lookup = 'passport_code'
    template_name = 'guest/card.html'

    def get_object(self, queryset=None):
        obj = get_object_or_404(Guest, passport_code=self.kwargs.get(self.lookup))
        obj.printed = True
        obj.save()
        return obj


class SwapBed(UserPassesTestMixin, TemplateView):
    template_name = 'panel/swap_bed.html'
    login_url = '/clinic/'

    def test_func(self):
        return self.request.user.is_superuser

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(SwapBed, self).dispatch(*args, **kwargs)

    def post(self, request, *args, **kwargs):
        source = request.POST.get('source')
        destination = request.POST.get('destination')

        if (source is None) or (destination is None):
            messages.error(request, _("Source or destination could not be empty"))
            return redirect(reverse('swap_bed'))

        try:
            source_bed = Bed.objects.select_related('tent').get(title=source)
            destination_bed = Bed.objects.select_related('tent').get(title=destination)
        except Bed.DoesNotExist:
            messages.error(request, _("Wrong bed ids entered"))
            return redirect(reverse('swap_bed'))

        if source_bed.status != Bed.UNAVAILABLE:
            messages.error(request, _("Source does not have any guest"))
            return redirect(reverse('swap_bed'))

        if destination_bed.status == Bed.SUSPENDED:
            messages.error(request, _("Destination bed is suspended"))
            return redirect(reverse('swap_bed'))

        with transaction.atomic():
            destination_guest = destination_bed.guest
            source_guest = source_bed.guest
            GuestBed.objects.filter(
                bed_id__in=[source_bed.id, destination_bed.id]
            ).delete()
            if destination_bed.guest:
                destination_log = EntryLog.objects.create(
                    guest=destination_guest,
                    log_type=EntryLog.SWAP,
                    operator=request.user,
                    bed=source_bed
                )
                destination_gb = GuestBed.objects.create(
                    tent=source_bed.tent, bed=source_bed,
                    guest=destination_guest, log=destination_log
                )
            else:
                source_bed.set_available()
                destination_bed.set_unavailable()

            source_log = EntryLog.objects.create(
                guest=source_guest,
                log_type=EntryLog.SWAP,
                operator=request.user,
                bed=destination_bed
            )
            source_gb = GuestBed.objects.create(
                tent=destination_bed.tent, bed=destination_bed,
                guest=source_guest, log=source_log
            )

            messages.success(request, _("Provided beds swapped successfully"))
            return redirect(reverse('swap_bed'))
