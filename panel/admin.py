from django.contrib import admin
from django.utils.html import format_html

from django.utils.translation import ugettext_lazy as _
from rest_framework.reverse import reverse

from guest.models import GuestBed
from panel.models import WhiteListGuest

from import_export import resources
from import_export.fields import Field

card_print_button = '<a class="button" href="{}" target=_blank>پرینت کارت</a>&nbsp;'


class TentResource(resources.ModelResource):
    fullname = Field()
    bed = Field()
    national_code = Field()

    class Meta:
        model = GuestBed
        fields = ('tent', 'bed', 'fullname', 'national_code')
        export_order = ('tent', 'bed', 'fullname', 'national_code')

    def dehydrate_fullname(self, obj):
        return '%s %s' % (obj.guest.first_name, obj.guest.last_name)

    def dehydrate_national_code(self, obj):
        return obj.guest.national_code

    def dehydrate_bed(self, obj):
        return obj.bed.title


class WhiteListGuestAdmin(admin.ModelAdmin):
    list_display = ("fullname", "passport_code", "national_code",
                    "mobile_number", 'print_btn')

    actions = ('remove_from_whitelist',)
    search_fields = (
        'national_code', 'first_name', 'last_name',
        'passport_code', 'mobile_number'
    )

    def print_btn(self, obj):
        url = reverse('card_detail', kwargs={'national_code': obj.national_code})
        return format_html(card_print_button.format(url))
    print_btn.short_description = _("Print card")

    def remove_from_whitelist(self, request, queryset):
        queryset.update(white_list=False)
    remove_from_whitelist.short_description = _("Remove selected user from whitelist")

    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False


admin.site.register(WhiteListGuest, WhiteListGuestAdmin)
admin.site.site_title = _("Mokeb")
admin.site.site_header = _("Mokeb Management")
admin.site.site_url = '/panel'
