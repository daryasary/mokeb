from rest_framework import serializers

from allocation.models import Allocation


class AllocationLiveSerializer(serializers.ModelSerializer):
    class Meta:
        model = Allocation
        fields = ('id', 'title', 'get_stats')
