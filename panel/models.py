from django.db import models
from django.utils.translation import ugettext_lazy as _

from guest.models import Guest


class WhiteListManger(models.Manager):
    def get_queryset(self):
        return super().get_queryset().filter(white_list=True)


class WhiteListGuest(Guest):
    """Simple proxy of guest model to handle whitelist users and provide specific tools"""
    objects = WhiteListManger()

    class Meta:
        proxy = True
        verbose_name = _("WhiteListGuest")
        verbose_name_plural = _("WhiteListGuests")
