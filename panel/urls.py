from django.conf.urls import url

from .views import (TentListView, TentDetailView,
                    AllocationLiveListView, PanelMainView,
                    AllocationView, UnPrintedGuestDetail, UnPrintedGuests,
                    SwapBed)

urlpatterns = [
    url(r'^$', PanelMainView.as_view(), name='panel'),
    url(r'^tents/$', TentListView.as_view(), name='tents_list'),
    url('^tents/(?P<slug>\d+)', TentDetailView.as_view(), name='tents_detail'),
    url(r'^allocations-live/$', AllocationLiveListView.as_view()),
    url(r'^allocations/$', AllocationView.as_view(), name='allocations'),
    url(r'^card/$', UnPrintedGuests.as_view(), name='card_list'),
    url(r'^swap/$', SwapBed.as_view(), name='swap_bed'),
    url(r'^card/(?P<passport_code>.+)/$', UnPrintedGuestDetail.as_view(), name='card_detail'),
]
