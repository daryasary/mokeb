��    �      �      �
      �
     �
  )     .   -     \     |  	   �  
   �     �     �  	   �  9   �  e   �     V     s          �     �     �     �     �     �     �  q   �     Y     b     i     r     ~     �     �     �     �  
   �     �     �     �     �       )   -  
   W     b     n     s     �  	   �     �     �  	   �  	   �     �     �     �     �     �     �  /        F  ,   T     �     �     �     �     �     �  "   �  
   �        "        /  
   8  #   C  ]   g     �     �  
   �     �     �  -        0  (   O     x  
   ~     �  	   �     �     �     �     �     �  	   �  
     *        >     J  $   d  +   �  	   �     �     �     �     �     �     �            
   %     0     <     C     G     L     U     ^     k  	   r     |     �     �     �     �  	   �     �     �  
   �     �     �  ,   �  	     
   "     -     4     :     A     T  	   ]     g     p     t     }     �     �     �  	   �     �     �     �     �     �     �     �     �     �     �     �               '  
   -     8     P     U     [     `     f     k     x     }     �     �  	   �  
   �     �  �  �      U  G   v  <   �  M   �  
   I     T     d     x     �     �  v   �  �     &   �     
          %     4     =  /   M     }     �  9   �  �   �     d     q     z     �     �     �     �     �  F   �     )     0     H  
   U  L   `  L   �  G   �     B     X     n     w     �     �     �     �  
   �     �               %     ;     D  *   Z  G   �     �  9   �          #     =     S     n     �  8   �     �     �  _   �     N      `   @   n   �   �      1!  J   E!  
   �!     �!     �!  T   �!  G   !"  <   i"     �"     �"  
   �"     �"  
   �"  6   �"  6   &#  J   ]#  
   �#     �#     �#  H   �#      $  9   :$  8   t$  A   �$     �$     %     %     -%     ?%      \%  <   }%     �%     �%  '   �%  '   &     9&     F&     M&     [&  
   o&     z&     �&     �&  
   �&     �&     �&     �&     �&     '     '     "'     +'  
   ;'     F'  �   S'     �'     �'  
   �'  
   �'     (  /   (     C(     U(     m(     v(     }(     �(     �(     �(     �(     �(     �(     �(     �(     )     )  
   *)     5)     R)     _)     s)  G   �)     �)  3   �)  
   *     *  K   )*     u*     ~*     �*  
   �*     �*     �*  
   �*     �*     �*     �*  
   
+     +  B   '+    By %(filter_title)s  A user with that username already exists. A user with this mobile number already exists. Add selected users to whitelist Admin Available Birth date Change password City Clearance Designates whether the user can log into this admin site. Designates whether this user should be treated as active. Unselect this instead of deleting accounts. Destination bed is suspended Device UUID Doctor Documentation Drug Drugs Email has already been set. End date Enter Enter a valid mobile number. Enter a valid username starting with a-z. This value may contain only letters, numbers and underscore characters. Entities Entity Entrance Examination ExaminationDrug ExaminationDrugs Examinations Exit Export selected tent guests First name Force Settlement Guardian Guest Guest doesn't have any bed Guest doesnt have any bed Guest with provided data does not exists. Guests Bed Guests Beds Home Home phone number Hospital Hospitals Implicit Bed Important dates Inspector Last name Log out Mobile number Modified Mokeb Mokeb Management Multiple tent selected Must include "{username_field}" and "password". National code No %(verbose_name)s found matching the query Notification Token Passport code Passport pic Permissions Personal info Pharmacy Phone Number has already been set. Print card Profile pic Provided beds swapped successfully Real Bed Registered Remove selected user from whitelist Required. 30 characters or fewer starting with a letter. Letters, digits and underscore only. Scopes Selected user already has bed Settlement Shift Shifts Sorry selected bed is not available currently Source does not have any guest Source or destination could not be empty Staff Start date State Suspended Swap There is no available bed now. There is not available bed now. This guest had bed before. Title Transport Transports Unable to login with provided credentials. Unavailable User account is disabled. User with this Email already exists. User with this Phone Number already exists. View site Vip Vip Inspector Welcome, WhiteListGuest WhiteListGuests Wrong bed ids entered accepted_terms active allocation allocations avatar bed beds birthday capacity card printed cities city name created time date joined description device devices diagnosis doctor drug drugs_name email address examination female is False, male is True, null is unset first bed first name gender guest guests invalid bed titles last bed last name location log log type logs mobile number modified time name nick_name operator panel priority profile profiles quantity role scope scopes shift source doesnt have any guest staff staff status state state name swap is only for guests tent tents time title type type of tent user username users verification codes warehouse white list you select a suspended bed Project-Id-Version: 
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2019-10-10 15:41+0330
PO-Revision-Date: 2019-10-10 15:41+0326
Last-Translator: b'  <hosein@mail.com>'
Language-Team: 
Language: fa
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: Poedit 1.8.7.1
X-Translated-Using: django-rosetta 0.9.3
 به وسیله %(filter_title)s کاربری با نام کاربری داده شده موجود است این شماره موبایل قبلا ثبت شده است اضاقه کردن مهمانان انتخاب شده به لیست سفید ادمین در دسترس تاریخ تولد تغییر گذرواژه شهر خروج با استفاده از این فیلد امکان ورود کاربر به صفحه ادمین تعیین میشود به کمک این فیلد تعیین میشود یا کاربر فعال بوده یا خیر!بجای ‍پاک کردن حساب کاربری کاربر از این فیلد استفاده کنید تخت مقصد معلق شده است UUID دستگاه پزشک مستندات دارو دارو‌ها این ایمیل قبلا ثبت شده است تاریخ پایان ورود یک شماره موبایل معتبر وارد کنید یک نام کاربری معتبر به صورت شروع با یکی از کارکترهای a-z و فقط شامل عدد-الفبا و ـ کالاها کالا ورود معاینه دارو‌ی معاینه دارو‌های معاینه معاینات خروج گرفتن خروجی از مهمانان چادر انتخاب شده نام تخلیه اجباری نگهبان مهمان به این مهمان هیچ تختی اختصاص داده نشده است به این مهمان هیچ تختی اختصاص داده نشده است مهمانی با داده‌های وارد شده وجود ندارد تخت مهمانان تخت مهمانان خانه شماره تلفن مرکز‌ پزشکی مرکز پزشکی تخت مجازی تاریخ‌های مهم بازرس نام خانوادگی خروج شماره همراه تغییر یافته موکب مدیریت موکب چند چادر انتخاب شده است هر دو فیلد "{username_field}" و "password" الزامی است کد ملی کاربری با نام %(verbose_name)s یافت نشد کد اعلان شماره گذرنامه عکس گذرنامه سطح دسترسی‌ها اطلاعات شخصی داروخانه این شماره تلفن قبلا ثبت شده است چاپ کارت عکس شخص مهمان‌های تخت‌های مورد نظر با موفقیت جابه‌جا شدند. تخت واقعی ثبت شده حذف کاربران انتخاب شده از لیست سفید این فیلد الزامی است. به صورت حداکثر 30 کاراکتر به صورت عدد و حرف الفبا و ـ محدوده‌ها به مهمان مورد نظر قبلا تخت اختصاص شده است تخلیه دوره اقامت دوره‌های اقامت متاسفانه تخت انتخابی در حال حاضر در دسترس نیست تخت مبدا به مهمانی اختصاص داده نشده است تخت مبدا و مقصد هردو باید پر شوند. کارکنان تاریخ شروع استان معلق شده تعویض در حال حاضر تختی در دسترس نیست در حال حاضر تختی در دسترس نیست این مهمان قبلا هم در موکب اقامت داشته است عنوان ورود/خروج ورود/خروج ورود با مشخصات وارد شده امکان پذیر نیست. غیرقابل دسترس این حساب کاربری غیرفعال شده است کاربری با این ایمیل ثبت شده است کاربری با این شماره تلفن ثبت شده است مشاهده سایت مهمان ويژه بازرس ویژه خوش آمدید لیست سفید مهمان لیست سفید مهمانان تختی با اطلاعات وارد شده یافت نشد امضای تعهدات آیا فعال است گروه‌های تخصیص یافته گروه‌های تخصیص یافته آواتار تخت تخت‌ها تاریخ تولد ظرفیت کارت چاپ شده شهر‌ها نام شهر تاریخ تاریخ ساخت حساب توضیحات دستگاه دستگاه‌ها تشخیص پزشک پزشک دارو نام دارو ایمیل معاینه خانم دارای مقدار false و آقا دارای مقدار true می باشد.null
 به معنای نامعین است اولین تخت نام جنسیت مهمان مهمانان تختی با این نام وجود ندارد آخرین تخت نام خانوادگی مکان لاگ نوع لاگ لاگ‌ها شماره موبایل تغییر یافته نام نام مستعار اپراتور پنل اولویت پروفایل پروفایل‌ها تعداد سطح دسترسی (نقش) محدوده محدوده‌ها دوره اقامت تخت مبدا به مهمانی اختصاص داده نشده است خادم آیا از نیرو‌های خدماتی است؟ استان نام استان مبادله تخت فقط برای مهمانان عادی ممکن است چادر چادرها زمان عنوان عملیات نوع چادر کاربر نام کاربری کاربران کد‌های فعال سازی انبار لیست سفید تخت انتخابی به حالت تعلیق درآمده است 