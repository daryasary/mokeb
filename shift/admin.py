from django.contrib import admin

from shift.models import Shift


class ShiftAdmin(admin.ModelAdmin):
    list_display = ['title', 'start_date', 'end_date']


admin.site.register(Shift, ShiftAdmin)
