from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _


class ShiftConfig(AppConfig):
    name = 'shift'
    verbose_name = _("shift")
