from rest_framework import serializers

from api.serializers import GuestSerializer
from guest.models import Guest, EntryLog
from shift.models import Shift


# class GuestSerializer(serializers.ModelSerializer):
#     pass


class ShiftReportSerializer(serializers.ModelSerializer):
    guests = serializers.SerializerMethodField()

    class Meta:
        model = Shift
        fields = ('title', 'start_date', 'end_date', 'guests')

    def get_guests(self, obj):
        guests = Guest.objects.filter(
            logs__time__lte=obj.end_date
        ).exclude(
            logs__log_type__in=[
                EntryLog.SETTLEMENT, EntryLog.FORCE_SETTLEMENT
            ]
        ).distinct()
        return GuestSerializer(guests, many=True).data
