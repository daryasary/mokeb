from django.db import models
from django.utils.translation import ugettext_lazy as _


class Shift(models.Model):
    title = models.CharField(verbose_name=_('Title'), max_length=32)
    start_date = models.DateTimeField(verbose_name=_('Start date'))
    end_date = models.DateTimeField(verbose_name=_('End date'))

    def __str__(self):
        return self.title

    class Meta:
        ordering = ['start_date']
        verbose_name = _('Shift')
        verbose_name_plural = _('Shifts')
        db_table = 'shift'
