from django.db import models
from django.utils.translation import ugettext_lazy as _


class Entity(models.Model):
    name = models.CharField(max_length=255, verbose_name=_("name"))
    description = models.TextField(blank=True, verbose_name=_("description"))

    created_at = models.DateTimeField(verbose_name=_('Registered'), auto_now_add=True)
    modified_at = models.DateTimeField(verbose_name=_('Modified'), auto_now=True)

    class Meta:
        verbose_name = _("Entity")
        verbose_name_plural = _("Entities")

    def __str__(self):
        return self.name


class Transport(models.Model):
    ENTRANCE = 1
    CLEARANCE = 2
    TRANSPORT_CHOICES = (
        (ENTRANCE, _("Entrance")),
        (CLEARANCE, _("Clearance")),
    )
    entity = models.ForeignKey(Entity, related_name='transports')
    transport_type = models.PositiveSmallIntegerField(choices=TRANSPORT_CHOICES, default=ENTRANCE, verbose_name=_("type"))
    quantity = models.IntegerField(verbose_name=_("quantity"))
    staff = models.CharField(max_length=255, verbose_name=_("staff"), null=True)

    created_at = models.DateTimeField(verbose_name=_('Registered'), auto_now_add=True)
    modified_at = models.DateTimeField(verbose_name=_('Modified'), auto_now=True)

    class Meta:
        verbose_name = _("Transport")
        verbose_name_plural = _("Transports")

    def save(self, *args, **kwargs):
        if self.transport_type == self.ENTRANCE:
            self.quantity = abs(self.quantity)
        else:
            self.quantity = -1 * abs(self.quantity)
        return super().save(*args, **kwargs)

