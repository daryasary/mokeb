# -*- coding: utf-8 -*-
# Generated by Django 1.11.9 on 2019-09-26 11:14
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('warehouse', '0003_auto_20190926_1105'),
    ]

    operations = [
        migrations.AlterField(
            model_name='transport',
            name='quantity',
            field=models.IntegerField(verbose_name='quantity'),
        ),
    ]
