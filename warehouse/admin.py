from django.contrib import admin
from django.db.models import Sum
from django.db.models.functions import Coalesce
from django.utils.translation import ugettext_lazy as _

from warehouse.models import Entity, Transport


class TransportInline(admin.TabularInline):
    model = Transport
    extra = 1


class EntityAdmin(admin.ModelAdmin):
    list_display = ['name', 'description', 'quantity', 'created_at']
    inlines = (TransportInline,)

    def quantity(self, obj):
        return obj.transports.aggregate(t=Coalesce(Sum('quantity'), 0))['t']
    quantity.short_description = _("quantity")


admin.site.register(Entity, EntityAdmin)
