from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _


class WarehouseConfig(AppConfig):
    name = 'warehouse'
    verbose_name = _("warehouse")
    verbose_name_plural = _("warehouse")
