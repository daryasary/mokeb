from django.contrib.auth import get_user_model
from django.db import models
from django.db.models import Sum
from django.db.models.functions import Coalesce
from django.utils.translation import ugettext_lazy as _

from guest.models import Guest


User = get_user_model()


class Drug(models.Model):
    name = models.CharField(verbose_name=_("name"), max_length=64)
    description = models.TextField(verbose_name=_("description"), blank=True)

    created_time = models.DateTimeField(verbose_name=_("created time"), auto_now_add=True)
    modified_time = models.DateTimeField(verbose_name=_("modified time"), auto_now=True)

    class Meta:
        verbose_name = _("Drug")
        verbose_name_plural = _("Drugs")

    def __str__(self):
        return self.name

    @property
    def quantity(self):
        return self.entities.aggregate(s=Coalesce(Sum('quantity'), 0))['s'] - self.examinations.aggregate(s=Coalesce(Sum('quantity'), 0))['s']


class Pharmacy(models.Model):
    drug = models.ForeignKey(Drug, verbose_name=_("drug"), related_name='entities')
    quantity = models.PositiveSmallIntegerField(verbose_name=_("quantity"))

    created_time = models.DateTimeField(verbose_name=_("created time"), auto_now_add=True)
    modified_time = models.DateTimeField(verbose_name=_("modified time"), auto_now=True)

    class Meta:
        verbose_name = _("Pharmacy")
        verbose_name_plural = _("Pharmacy")

    def __str__(self):
        return "{}: {}".format(self.drug, self.quantity)


class Examination(models.Model):
    guest = models.ForeignKey(Guest, verbose_name=_("guest"), related_name='examinations')
    diagnosis = models.TextField(blank=True, verbose_name=_("diagnosis"))
    drugs = models.ManyToManyField(Drug, through="ExaminationDrug")

    user = models.ForeignKey(User, related_name='examinations', blank=True, null=True, editable=False)
    created_time = models.DateTimeField(verbose_name=_("created time"), auto_now_add=True)
    modified_time = models.DateTimeField(verbose_name=_("modified time"), auto_now=True)

    class Meta:
        verbose_name = _("Examination")
        verbose_name_plural = _("Examinations")


class ExaminationDrug(models.Model):
    examination = models.ForeignKey(Examination, verbose_name=_("examination"))
    drug = models.ForeignKey(Drug, verbose_name=_("drug"), related_name='examinations')
    quantity = models.PositiveSmallIntegerField(verbose_name=_("quantity"))

    created_time = models.DateTimeField(verbose_name=_("created time"), auto_now_add=True)
    modified_time = models.DateTimeField(verbose_name=_("modified time"), auto_now=True)

    class Meta:
        verbose_name = _("ExaminationDrug")
        verbose_name_plural = _("ExaminationDrugs")
