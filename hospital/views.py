from django.views.generic import TemplateView, ListView

from hospital.models import Examination, Drug


class ClinicMainView(TemplateView):
    """
    Main view of panel should provide a list of all available urls for
    web operator of system urls may modify manually
    """

    template_name = 'hospital/clinic_panel.html'


class ExaminationsListView(ListView):
    model = Examination
    queryset = Examination.objects.select_related('guest').all().order_by('-created_time')
    template_name = 'hospital/examination_list.html'


class DrugStoreListView(ListView):
    model = Drug
    queryset = Drug.objects.all().order_by('-id')
    template_name = 'hospital/drugstore.html'
