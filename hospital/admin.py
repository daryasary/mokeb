from django.contrib import admin
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.utils.translation import ugettext_lazy as _

from hospital.models import ExaminationDrug, Drug, Pharmacy, Examination


class DrugAdmin(admin.ModelAdmin):
    list_display = ['name', 'quantity']


class PharmacyAdmin(admin.ModelAdmin):
    list_display = ['drug', 'quantity', 'created_time']

    def has_change_permission(self, request, obj=None):
        return request.user.is_superuser


class ExaminationDrugInline(admin.TabularInline):
    model = ExaminationDrug
    raw_id_fields = ["drug"]


class ExaminationAdmin(admin.ModelAdmin):
    list_display = ['guest', 'doctor', 'diagnosis', 'drugs_name', 'created_time']
    inlines = [ExaminationDrugInline]
    raw_id_fields = ['guest']

    def drugs_name(self, obj):
        return ', '.join(obj.drugs.values_list('name', flat=True))
    drugs_name.short_description = _("drugs_name")

    def doctor(self, obj):
        return obj.user
    doctor.short_description = _("doctor")

    def save_model(self, request, obj, form, change):
        obj.user = request.user
        return super().save_model(request, obj, form, change)

    def response_add(self, request, obj, post_url_continue=None):
        return HttpResponseRedirect(reverse("examinations"))

    def response_change(self, request, obj):
        return HttpResponseRedirect(reverse("examinations"))


admin.site.register(Drug, DrugAdmin)
admin.site.register(Pharmacy, PharmacyAdmin)
admin.site.register(Examination, ExaminationAdmin)
