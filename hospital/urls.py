from django.conf.urls import url

from hospital.views import ClinicMainView, ExaminationsListView, DrugStoreListView

urlpatterns = [
    url(r'^$', ClinicMainView.as_view(), name='clinic'),
    url(r'^examinations/$', ExaminationsListView.as_view(), name='examinations'),
    url(r'^drugstore/$', DrugStoreListView.as_view(), name='drugstore'),
]
