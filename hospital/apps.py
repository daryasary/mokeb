from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _


class HospitalConfig(AppConfig):
    name = 'hospital'
    verbose_name = _("Hospital")
    verbose_name_plural = _("Hospitals")
