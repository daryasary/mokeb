from django.db import models
from django.utils.translation import ugettext_lazy as _


class State(models.Model):
    state_name = models.CharField(_('state name'), max_length=100)

    class Meta:
        db_table = "locations_states"
        ordering = ["state_name"]

    def __str__(self):
        return self.state_name


class City(models.Model):
    state = models.ForeignKey(State, verbose_name=_('state'))
    city_name = models.CharField(_('city name'), max_length=150)

    class Meta:
        db_table = "locations_cities"
        ordering = ["city_name"]
        verbose_name_plural = _('cities')

    def __str__(self):
        return '%s - %s' % (self.state, self.city_name)
